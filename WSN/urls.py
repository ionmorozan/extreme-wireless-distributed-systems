from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'WSN.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #home page
    url(r'^$', 'wsn.views.index'),

    #info
    url(r'^info', 'wsn.views.info'),

    #contact
    url(r'^contact', 'wsn.views.contact'),

    #blog
    url(r'^blog', 'wsn.views.blog'),

    #blog posts - match every page of this form {ab} a,b ={0..9}
    url(r'^(\d{2})', 'wsn.views.blog_posts'),

    #demo - map
    url(r'^map', 'wsn.views.map'),

    #admin panel
    url(r'^admin/', include(admin.site.urls)),
)