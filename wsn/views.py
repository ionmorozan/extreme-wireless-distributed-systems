from django.shortcuts import render_to_response
from django.http import HttpResponse

from wsn.models import Hotspots
from wsn.models import hotspotsHistory
from wsn.models import detectionsHistory

import datetime
import json

from datetime import datetime, timedelta


def index(request):
    return render_to_response("index.html", {})


def info(request):
    return render_to_response("info.html", {})

def contact(request):
    return render_to_response("contact.html", {})

def blog(request):
    return render_to_response("blog.html", {})

#return certain blog from the site
def blog_posts(request, post_nr):
    return render_to_response(post_nr + ".html", {})


def map(request):
    #-------------------------------------------HOTSPOTS---------------------------------------
    #get all hotspots
    allHotspots =[]
    #allHotspots = Hotspots.objects.all()
    allHotspots = Hotspots.objects.using('arnhem_data').all()

    #create a list with all hotspots from database
    hotspots = []

    #get the hotspots that have been added in the past minute
    #need this to display on the map the hotspots with real time information
    now = datetime.now()
    earlier = now - timedelta(minutes=1)
    #pastMinuteHotspot = hotspotsHistory.objects.filter(time__range=(earlier,now))
    pastMinuteHotspot = hotspotsHistory.objects.using('arnhem_data').filter(time__range=(earlier,now))


    #print pastMinuteHotspot
    for spot in allHotspots:
        switch = 0;
        for minute_spot in pastMinuteHotspot:
            if spot.id == minute_spot.hotspot_id:
                switch = 1
                no_dev = int(minute_spot.num_devices)
                color = 'blue'
                if no_dev > 50 and no_dev < 100:
                    color = 'green'
                elif no_dev > 100 :
                    color = 'red'
                hotspots.append([spot.latitude, spot.longitude, str(spot.name), int(minute_spot.num_devices), color, int(spot.id), int(spot.radius)])
                break
        if switch == 0:
            hotspots.append([spot.latitude, spot.longitude, str(spot.name), 0, 'blue', int(spot.id), int(spot.radius)])

    #print hotspots

    #-------------------------------------------DEVICES---------------------------------------
    #create a list with all trajectories for each device
    allPaths = {}

    #get current time
    now = datetime.now()

    #GET PAST MINUTE DEV(10 minutes delay)
    past = now - timedelta(minutes=11) #17:45
    past_minus_one = now - timedelta(minutes=10) #17:46

    print 'past: '  +  str(past)
    print 'past - 1: '  +  str(past_minus_one)

    #pastMinuteDevices = detectionsHistory.objects.filter(time__range=(past, past_minus_one))
    pastMinuteDevices = detectionsHistory.objects.using('arnhem_data').filter(time__range=(past, past_minus_one))

    for device in pastMinuteDevices:
        allPaths[str(device.device_id)] = []

    for device in pastMinuteDevices:
        allPaths[str(device.device_id)].append([device.latitude, device.longitude, str(device.time), str(past), int(device.radius*4)])

    for device in pastMinuteDevices:
        allPaths[str(device.device_id)].sort(key=lambda tup: tup[2]);

    #print allPaths

    #send two json objects into one json file
    response_data = {}
    response_data['hotspots'] = hotspots
    response_data['paths'] = allPaths


    #print response_data
    if request.is_ajax():
        print "is Ajax"
        return HttpResponse(json.dumps(response_data))
    else:
        return render_to_response("map.html", {'hotspots':hotspots, 'paths':allPaths})



#return information about the hotspot was clicked on
def pastHourHotspots(request, h_name=None):

    #h_name holds the id of the hotspot
    hotspots = []

    #get the hotspots acitvity in the past hour
    now = datetime.now()
    earlier = now - timedelta(hours=1)
    pastHourHotspots = hotspotsHistory.objects.using('arnhem_data').filter(time__range=(earlier,now), hotspot_id=h_name)

    #add the number of seen devices during the past hour
    for spot in pastHourHotspots:
        hotspots.append(spot.num_devices)


    hotspots = json.dumps(hotspots)

    return HttpResponse(hotspots)