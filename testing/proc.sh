#!/bin/bash
#set this script to run only on 0 cpu (first cpu)
#run this script like this: taskset -c 0 ./proc.ch

truncate -s 0 cpu_avg
truncate -s 0 mem_avg

refresh_rate=0.1

#run testing script
#python insert_data.py > output  &

#function which gets the cpu load of the entire system
get_load_cpu(){
	read cpu a b c previdle_all rest < /proc/stat
	prevtotal_all=$((a+b+c+previdle_all))


	sleep $refresh_rate

	read cpu a b c idle_all rest < /proc/stat
	total_all=$((a+b+c+idle_all))
	CPU=$((100*( (total_all-prevtotal_all) - (idle_all-previdle_all) ) / (total_all-prevtotal_all)))
	#echo $CPU
}

#get information about memory occupancy
get_load_mem(){
	read info mem_total rest < /proc/meminfo
	read info mem_free rest <<<  `awk 'NR==2'  /proc/meminfo`
	mem_used=$((mem_total - mem_free))
	
	MEM=$(((mem_used * 100)/mem_total))
	#echo $MEM
}


#while [[ `pgrep -f "insert_data.py"` ]];
while [[ `pgrep -f "init"` ]];
do
	get_load_cpu
	get_load_mem
	
	
	if [ "$CPU" -gt 0 ] 
	then
		echo 'Load:' $((CPU)) '%'
		echo $((CPU)) >> cpu_avg

		echo 'Mem:' $((MEM)) '%'
		echo $((MEM)) >> mem_avg
#	else
#		break
	fi
done

echo 'AVG:' `awk '{ total += $1 } END { print total/NR }' cpu_avg` '%'
echo 'MEM AVG:' `awk '{ total += $1 } END { print total/NR }' mem_avg` '%'

