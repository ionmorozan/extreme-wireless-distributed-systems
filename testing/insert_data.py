'''
	This file inserts artificial data to mysql database.
	Its purpose is testing only, to measure cpu load and memory. 

	Author: Ion Morozan
	<i.morozan@student.vu.nl>
'''
import urllib
import urllib2
import json
import time

BASE = 'http://goflow.distributed-systems.net/arnhem/postDetection?'

devices = []
nr_devices = 180
nr_hotspots = 6

#POST request to web application
def make_request(url, values=''):
	
	data = json.dumps(values)
	req = urllib2.Request(url, data)
	#req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
	response = urllib2.urlopen(req)
	the_page = response.read()
	response.close()
	
	print the_page

#small scenario
def scenario(hotspot, offset):
	global nr_devices, nr_hotspots, devices
	
	#nr of devices per small group
	rounds = nr_devices / nr_hotspots

	URL = BASE + 'h=' + hotspot
	for d in range(rounds * offset, rounds * (offset+1)):
		URL += '&d[]=' + devices[d]
	URL += '&s=1'

	print URL
	print len(URL)
	#make_request(URL, '')

#big scenario
def init_scenario(hotspot):
	global nr_devices, nr_hotspots, devices

	URL = BASE + 'h=' + hotspot
	for device in devices:
		URL += '&d[]=' + device
	URL += '&s=1'

	print URL
	print len(URL)
	#make_request(URL, '')



def main():
	
	global nr_devices, nr_hotspots, devices
	s = 1

	#create a dictionary with hotspots mac
	hotspots = {}
	hotspots['RID'] ='a8:54:b2:92:fc:cc'
	hotspots['Skar'] ='a8:54:b2:92:fd:6c'
	hotspots['Haarhuis'] ='a8:54:b2:92:fc:ae'
	hotspots['Wilke'] ='a8:54:b2:92:fd:04'
	hotspots['barXO'] ='a8:54:b2:92:fc:ac'
	hotspots['wifituin'] ='a8:54:b2:92:fd:54'
	
	#print hotspots
	
	
	#http://goflow.distributed-systems.net/arnhem/postDetection?h=a8:54:b2:92:fd:6c&d[]=dev1&d[]=dev2&d[]=dev3&s=1
	#for all the hotspots
	rounds = 0
	for hotspot in hotspots.values():
		#create devices
		del devices[:]
		for i in range(nr_devices*rounds, nr_devices*(rounds+1)):
			devices.append('d'+str(i))
	

		#simulate a crowd detected by a hotspot
		init_scenario(hotspot)

		#wait people to reach other hotspots
		time.sleep(150)

		#the crowd is divided in smaller groups to other hotspots
		offset = 0
		for hotspotT in hotspots.values():
			#of course not the one that it started
			if hotspot != hotspotT:
				#send small groups
				scenario(hotspotT,offset)
				offset+=1;
		
		#go to next round
		rounds+=1

if __name__ == "__main__":
	main()

