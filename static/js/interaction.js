$(document).ready(function () {

    $(document).tooltip();
	

    $("#hotspotshead").click(function () {
        $("#hotspotlist").toggle('slide', {
            direction: 'up'
        }
        /* , function () {
        if ($("#hotspotswrap").is(":visible")) {
            $("#hotspotsviewbutton").css("background", "#599EFF");
            $("#hotspotsviewbutton").css("color", "#FFF");
        } else {
            $("#hotspotsviewbutton").css("background", "#FFF");
            $("#hotspotsviewbutton").css("color", "#000");
        }
    } */
        );

    });

    //toggle between the views

    //trips view
	
    $("#tripsviewbutton").click(function () {
		if( $("#bg-lines").is(":visible")){
				return false;
				} else{
			
        $("#tripsviewbutton").css("background", "#599EFF");
        $("#tripsviewbutton").css("color", "#FFF");

        $("#liveviewbutton").css("background", "#FFF");
        $("#liveviewbutton").css("color", "#599EFF");


        $('#bg-lines').show('slide', {
            direction: 'down'
        }, 1000);
        $('#slider').show('slow');
        $("#hotspotswrap").show('slide', {
            direction: 'left'
        }, 1000);

      
            $('#realtimecontainer').toggle('slide', {
                direction: 'right'
            }, 1000, function () {
                $('#statscontainer').toggle('slide', {
                    direction: 'right'
                }, 1000)
            });

        }
		
		//Change map style to Trips style

        mapStyle = [{
            stylers: [{
                hue: '#FFCC33'
            }, {
                visibility: 'simplified'
            }, {
                gamma: 0.5
            }, {
                weight: 0.5
            }]
        }, {
            elementType: 'labels',
            stylers: [{
                visibility: 'on'
            }]
        }, {
            featureType: 'water',
            stylers: [{
                color: '#a4caff'
            }]
        }, {
    featureType: "administrative.country",
    elementType: "geometry.stroke",
    stylers: [
      { visibility: "on" },
      { weight: 2 },
      { color: "#333535" }
    ]
  }];
        MapOverlays.init(); // render map with new style
    });

    //live view
    $("#liveviewbutton").click(function () {
if( $("#bg-lines").is(":hidden")){
				return false;
				} else{
        $("#tripsviewbutton").css("background", "#FFF");
        $("#tripsviewbutton").css("color", "#599EFF");

        $("#liveviewbutton").css("background", "#599EFF");
        $("#liveviewbutton").css("color", "#FFF");


        $('#bg-lines').hide('slide', {
            direction: 'down'
        });
        $('#slider').fadeOut('slow');
        $("#hotspotswrap").hide('slide', {
            direction: 'left'
        }, 1000);


        if ($('#statscontainer').is(":visible")) {
            $('#statscontainer').toggle('slide', {
                direction: 'right'
            }, 1000, function () {
                $('#realtimecontainer').toggle('slide', {
                    direction: 'right'
                }, 1000)
            });
        } 
		
		}

	  //change map style to Live style on click
        mapStyle =

[
  {
    "featureType": "landscape",
    "stylers": [
      { "visibility": "on" },
      { "color": "#808080" }
    ]
  },{
    "featureType": "road",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "transit.station.rail",
    "stylers": [
      { "visibility": "on" }
    ]
  },{
    "featureType": "landscape",
    "stylers": [
      { "visibility": "on" },
      { "color": "#5a5678" },
      { "weight": 1.8 },
      { "saturation": 39 },
      { "lightness": -9 }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "visibility": "on" },
      { "color": "#808080" }
    ]
  },{
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      { "weight": 1.4 },
      { "visibility": "off" },
      { "hue": "#a1ff00" }
    ]
  },{
    "featureType": "administrative",
    "elementType": "labels",
    "stylers": [
      { "visibility": "on" },
      { "color": "#ffffff" },
      { "weight": 0.5 }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "color": "#d3cfd3" }
    ]
  },{
    "featureType": "transit.line",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "administrative.country",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "weight": 3 },
      { "color": "#FFDB38" }
    ]
  },{
    "featureType": "administrative.country",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "color": "#008080" },
      { "weight": 5.5 }
    ]
  }
]
        MapOverlays.init(); //render map with new style

    });
});

// submit data to server through the form asynch
$(function () {
    $("#realTime").click(function () {
        var name = $("input#username_id").val();
        var dataString = 'username_id=' + name;

        $.ajax({
            type: "POST",
            url: "/follow/",
            data: dataString,
            success: function (data) {
                $("input#username_id").val('');

                // if there is an answer, if the username is valid and public
                if (data) {
                    tweet_info = $.parseJSON(data);

                    pointA = tweet_info['pointA'];
                    // if (!pointA)
                    // pointA = "Amsterdam";
                    pointB = tweet_info['pointB']
                    name = tweet_info['name']
                    text = tweet_info['text']
                    photo = tweet_info['photo']

                    console.log(pointA);
                    console.log(pointB);
                    console.log(name);
                    console.log(text);

                    pointAtoB(pointA, pointB, name, text, photo);
                }

            }
        });
        return false;
    });
});