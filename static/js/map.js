var mapStyle = 

[{
            stylers: [{
                hue: '#FFCC33'
            }, {
                visibility: 'simplified'
            }, {
                gamma: 0.5
            }, {
                weight: 0.5
            }]
        }, {
            elementType: 'labels',
            stylers: [{
                visibility: 'on'
            }]
        }, {
            featureType: 'water',
            stylers: [{
                color: '#a4caff'
            }]
        }, {
    featureType: "administrative.country",
    elementType: "geometry.stroke",
    stylers: [
      { visibility: "on" },
      { weight: 2 },
      { color: "#333535" }
    ]
  }]
  
 
var MapOverlays = {
	map : null,
	markers : [],
	circles : []
};

// directions for real time tweet analysis
var directionsDisplay;

// total number of trips per week
var totalTrips = 0;
var onedaytrips = 0;

// current markers which bounces
var currentMarker = null;

// flag which signals if the stop button is pressed when
// week trips are displayed.
stop_pressed = false;

// Enable the visual refresh
google.maps.visualRefresh = true;

// One infoWindow for all the places
var infoWindow = new google.maps.InfoWindow({});
var infoWindowPointB = new google.maps.InfoWindow({});

// see if the the markers are rendered on the map or not
var enable = true;

// zoom of the map for predicted journeys
var zoom_val = 15;

// change perspective of the map when display A->B
var transitLayer = new google.maps.TransitLayer();

// for real time analysis A->B
var PointBMarker = null;

var markerPositions = [];

/**
 * Shows or hides all marker overlays on the map.
 */
MapOverlays.toggleMarkers = function(opt_enable) {
	if (typeof opt_enable == 'undefined') {
		opt_enable = !MapOverlays.markers[0].getMap();
	}
	for ( var n = 0, marker; marker = MapOverlays.markers[n]; n++) {
		marker.setMap(opt_enable ? MapOverlays.map : null);
	}
};

/**
 * Shows or hides all circles overlays on the map.
 */
MapOverlays.toggleCircles = function(opt_enable) {
	if (typeof opt_enable == 'undefined') {
		opt_enable = !MapOverlays.circles[0].getMap();
	}
	for ( var n = 0, circle; circle = MapOverlays.circles[n]; n++) {
		circle.setMap(opt_enable ? MapOverlays.map : null);
	}
};

MapOverlays.toggleAllOverlays = function() {
	enable = true;
	if (MapOverlays.markers[0].getMap()) {
		enable = false;
	}
	MapOverlays.toggleMarkers(enable);
	MapOverlays.toggleCircles(enable);
};

/**
 * Called only once on initial page load to initialize the map.
 */
var MY_MAPTYPE_ID = 'custom_style';

MapOverlays.init = function() {

	var featureOpts = mapStyle;
  
	// Create single instance of a Google Map.
	var mapOptions = {
		zoom : zoom_val,
		center : new google.maps.LatLng(51.985103,5.89873),
		disableDefaultUI : true,
		mapTypeControlOptions: {
		  mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
		},
		mapTypeId: MY_MAPTYPE_ID,
	};

	MapOverlays.map = new google.maps.Map(document.getElementById('map'), mapOptions);

	  var styledMapOptions = {
	    name: 'Custom Style'
	  };

	  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

	  MapOverlays.map.mapTypes.set(MY_MAPTYPE_ID, customMapType);


	Geolocate();

	// show all hotspots.
	MapOverlays.toggleAllOverlays();

    //create movable objects (people connected to hotspots)
    InitializeTracking(MapOverlays.map);

};

/* method being called every 60 sec to refresh the content of
 * the page refresh only parts of the page
 */
function CalledPeriodically(){
    //make an async call to DB to fetch new data if present
    $.ajax({
        url:'/map',  // a normal get request
        success:function(response_data){  // success is the callback when the server responds

            json_data = $.parseJSON(response_data);
//            console.log(json_data);

            //take from the json the values for hotposts and devices
            hotspots = json_data['hotspots']; // hotspots
            paths = json_data['paths']; // paths

//            console.log(hotspots);
//            console.log(paths);

            // if there are objects on the map, remove them otherwise no
            if (enable) {
                // remove logically the markers from the map
                MapOverlays.toggleAllOverlays();

                // remove physically
                ClearMap();
            }
            Geolocate();
            //update the content of hotspots bar
            updateHotSpotsBar();

            MapOverlays.toggleAllOverlays();

            //create movable objects (people connected to hotspots)
            InitializeTracking(MapOverlays.map);
        }
    });


}

function Geolocate() {
	// create the markers
	for ( var i = 0; i < hotspots.length; i++) {
		markerPositions[i] = new google.maps.LatLng(hotspots[i][0], hotspots[i][1]);
	}

	// create circles and place them(with markers) on the map
	for ( var n = 0, latLng; latLng = markerPositions[n]; n++) {
		/*-----------------CREATE MARKERS -----------------*/
		// create markers
		var marker = new google.maps.Marker({
			position : latLng,
			//animation : google.maps.Animation.DROP,
			icon : '/static/img/wifi.png'
		});
		/*-----------------END CREATE MARKERS -----------------*/

		/*-----------------BOUNCING MARKERS -----------------*/
		var onMarkerToggleClick = function() {

			if (currentMarker)
				currentMarker.setAnimation(null);

			currentMarker = this;

			if (currentMarker.getAnimation() != null) {
				currentMarker.setAnimation(null);
			} else {
				currentMarker.setAnimation(google.maps.Animation.BOUNCE);
			}
		}

		google.maps.event.addListener(marker, 'click', onMarkerToggleClick);
		/*-----------------END BOUNCING MARKERS -----------------*/

		/*-----------------INFO WINDOW-----------------*/
		// listener for marker to show info text
		var onMarkerClick = function() {
			var marker1 = this;

			var markerindex = MapOverlays.markers.indexOf(marker1);
			var name = hotspots[markerindex][2];
			var id_hotspot= hotspots[markerindex][5];
            console.log(name);
            var no_dev = hotspots[markerindex][3];
            var density = hotspots[markerindex][4];


			var latLng = marker1.getPosition();

            /* make a GET request to the API and get the number of connected
             * devices seen during the past hour
             */
            var xmlHttp = null;
            xmlHttp = new XMLHttpRequest();
            //xmlHttp.open( "GET", 'api/pastHourHotspots/' + name, false );
            xmlHttp.open( "GET", 'api/pastHourHotspots/' + id_hotspot, false );
            xmlHttp.send( null );
            var pastHourHotspots = JSON.parse(xmlHttp.responseText);

            //get max and min from the array
            var max = Math.max.apply(Math, pastHourHotspots); // get the max value
            var max_pos = pastHourHotspots.indexOf(max);
            var min = Math.min.apply(Math, pastHourHotspots); // get the max value
            var min_pos = pastHourHotspots.indexOf(min);

            //get the past hour as an interval
            var date = new Date();
            var hour = date.getHours();
            var minutes = date.getMinutes();
            var zero =""
            if (minutes < 10)
                zero ="0"
            pastHour = hour - 1 + ':' + zero + minutes + '+-+' + hour + ':' + zero + minutes


            /*shown information on the info window when clicked
             * plot graph with historic information
             * info: https://developers.google.com/chart/image/docs/gallery/line_charts#gcharts_chart_title
             */
            var tag = '&amp;';
            var chart = "http://chart.apis.google.com/chart?"
                + 'cht=lc' + tag  //line chart
                + 'chco=FF0000' + tag //color of the line
                + 'chs=500x220' + tag // dimension
                + 'chtt=Seen+devices+during+the+past+hour|' + pastHour + tag //title
                + 'chxt=x,x,y,y' + tag //axis
                + 'chxl=1:|Time+(1+hour)|3:|Nr.+devices' +tag
                + 'chxp=1,50|3,50' + tag
                + 'chxr=0,0,60,5|2,0,' + max + tag //labels for x and y axis
                + 'chdl=Max=' + max + '|Min=' + min + tag
                + 'chco=ff0000,0000ff' + tag
                //+ 'chxt=x' + tag //axis
                //+ 'chxr=0,0,60,5' + tag //labels for x and y axis
                + 'chg=20,20' + tag // grid lines
                + 'chd=t:' //10,15,180,23,40,167' + tag //data - no of devices

            //add values/data
            for(var i = 0; i<pastHourHotspots.length; i++){
                chart+=pastHourHotspots[i] + ','
            }
            chart = chart.slice(0, -1) + tag;
            //chart += 'chm=tMin,0000FF,0,' + min_pos + ',10|fMax,FF0000,0,' + max_pos + ',15' + tag //show the min value and max value
            chart += 'chm=s,FF0000,0,'+ max_pos + ',10|s,0000FF,0,'+ min_pos + ',10' + tag //point the min and the max nr of devices /red - amx . blue - min
//            chart += 'chm=N*,000000,0,-4,9' + tag //show values
            chart += 'chds=0,' + max + tag // scalling the axis for autoscaling use 'chds=a'



            console.log(chart);
            infoWindow.setContent('<h4>' + name + '</h4><br/>'
                + '<img src=' + chart + '/>'
                + '<br/><h5>Current nr of devices: '
                + no_dev + '</h5>')

			infoWindow.open(MapOverlays.map, marker1);
		};

		// add listener to our currently placed marker to close when press on the map
		google.maps.event.addListener(MapOverlays.map, 'click', function() {
			infoWindow.close();
		});

		google.maps.event.addListener(marker, 'click', onMarkerClick);
//        google.maps.event.addListener(marker, 'click', function() {
//            drawChart(this);
//        });
		/*----------------- END INFO WINDOW-----------------*/

		// add markers to array
		MapOverlays.markers.push(marker);

		/*-----------------CIRCLE-----------------*/
		// Construct the circle for each value in citymap.
		var circleOptions = {
			strokeColor : "transparent",
			strokeOpacity : 0.8,
			strokeWeight : 2,
			fillColor : hotspots[n][4], //density of devices,//'blue',
			fillOpacity : 0.2,
			map : MapOverlays.map,
			center : new google.maps.LatLng(latLng.lat(), latLng.lng()),
			radius : 100
		};
		circle = new google.maps.Circle(circleOptions);

		// Add circle to collection
		MapOverlays.circles.push(circle);
		/*-----------------END CIRCLE-----------------*/
	}

}

// Call the init function when the page loads.
google.maps.event.addDomListener(window, 'load', MapOverlays.init);

//update the view the list of hotspots with approipriate data
function updateHotSpotsBar() {

    $('#hotspotlist').empty();
    for ( var i = 0; i <hotspots.length; i++) {

        h_name = hotspots[i][2]; // name
        h_no_dev = hotspots[i][3]; //no_dev

        $('#hotspotlist').append(
            '<li>'
                + '#' + (i+1) + ' '
                + h_name + ' '
                + '<span class="numoftrips">' + h_no_dev
                + '</span>' +
            '</li>')

    }
}

function ClearMap() {
	// remove elements
	while (MapOverlays.markers.pop() != null)
		;
	while (MapOverlays.circles.pop() != null)
		;

	markerPositions = new Array();
}