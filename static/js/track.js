/**
 * User: Ion Morozan
 * Date: 3/13/14
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */



var lines = new Array();
var coordinates = new Array();

function InitializeTracking(map){

    var i = 0;

    //clear map of previous tracks
    clearDisplayofPolylines();

    //initialize paths
    var date = new Date();
    for(p in paths){
        coordinates[i] = new Array();

        var now = new Date(paths[p][0][3]);
	var pathDate = new Date(paths[p][0][2]);
			
	var time_to_start = pathDate - now;

	if(time_to_start < 0){
		time_to_start = 1;
		//console.log(new Date(paths[p][j][2])); //timestam
	}

        for(var j = 0; j < paths[p].length; j++){
		coordinates[i].push(new google.maps.LatLng(paths[p][j][0], paths[p][j][1]));
	}
	
	var pathDateFin = new Date(paths[p][paths[p].length-1][2]);
	//console.log('len ' + (paths[p].length-1) );
	var time_to_end = pathDateFin - now;
	if(time_to_end < 0){
		time_to_end = 1;
	}

	var duration = time_to_end - time_to_start;
	setTimeout( function(i){CreateObject(coordinates[i], map, "#FF0000", 0.00001, i, duration )}, time_to_start, i);

        i++;
    }
}

/**
 Create people trajectories
 * @param coordinates - list of lat and long representing the path
 * @param map - instance of the map
 * @param color - the color of the object
 * @param pathStroke - width of the path, if 0 then no path shown
 */
function CreateObject(coordinates, map, color, pathStroke, index, duration){
    // Define the symbol, using one of the predefined paths ('CIRCLE')
    // supplied by the Google Maps JavaScript API.
    var lineSymbol = {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 8,
        strokeColor: 'green'
    };

    // Create the polyline and add the symbol to it via the 'icons' property.
    lines[index] = new google.maps.Polyline({
        path: coordinates,
        icons: [{
            icon: lineSymbol,
            offset: '100%'
        }],
        geodesic: true,
        strokeColor: color,
        strokeWeight: pathStroke,
        map: map
    });

    //display trajectory(path)
    Display(lines[index], 250, duration);

}


/**
 * Use the DOM setInterval() function to change the offset of the symbol
 * at fixed intervals.
 * @param line - trajectory
 * @param speed - move speed
 */
function Display(line, speed, duration) {
    var count = 0;
    if (duration != 0){
	    window.setInterval(function() {
	        //count = (count + 1) % 200;
		count = count + 100 / (duration/speed);
 
	        var icons = line.get('icons');
	        icons[0].offset = count + '%' ; //(count / 2) + '%';
        	line.set('icons', icons);
    	}, speed);
     }
}

function clearDisplayofPolylines(){
    for (i = 0; i < lines.length; i++){
        lines[i].setMap(null);
    }
    lines = new Array();

}
